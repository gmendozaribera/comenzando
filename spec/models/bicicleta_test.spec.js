var Bicicleta = require("../../models/bicicleta");
const bicicleta = require("../../models/bicicleta");

beforeEach(() => {
  bicicleta.allBicis = [];
});
describe("bicicleta.allBicis", () => {
  it("comienza vacia", () => {
    expect(bicicleta.allBicis.length).toBe(0);
  });
});

describe("bicicleta.add", () => {
  it("agregamos una", () => {
    expect(bicicleta.allBicis.length).toBe(0);

    var a = new bicicleta(1, "rojo", "urbana", [-17.793284, -63.099103]);
    bicicleta.add(a);

    expect(bicicleta.allBicis.length).toBe(1);
    expect(bicicleta.allBicis[0]).toBe(a);
  });
});

describe("bicicleta.findBy", () => {
  it("buscamos id con id=1 : ", () => {
    expect(bicicleta.allBicis.length).toBe(0);

    var aBici = new Bicicleta(1, "verde", "urbana");
    var aBici2 = new Bicicleta(2, "roja", "montaña");

    bicicleta.add(aBici);
    bicicleta.add(aBici2);

    var targetBici = bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe("verde");
    expect(targetBici.modelo).toBe(aBici.modelo);
  });
});

describe("bicileta.remobeById", () => {
  it("eliminamos el id = 1", () => {
    var aBici = new Bicicleta(1, "verde", "urbana");
    var aBici2 = new Bicicleta(2, "roja", "montaña");

    bicicleta.add(aBici);
    bicicleta.add(aBici2);

    expect(bicicleta.allBicis.length).toBe(2);
    bicicleta.removeById(1);

    expect(bicicleta.allBicis.length).toBe(1);

    expect(bicicleta.allBicis[0].id).toBe(2);
    expect(bicicleta.allBicis[0].color).toBe(aBici2.color);
    expect(bicicleta.allBicis[0].modelo).toBe(aBici2.modelo);
  });
});
