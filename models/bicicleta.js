var bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + ' | color : ' + this.color;
}

bicicleta.allBicis = [];
bicicleta.add = function(aBici){
    bicicleta.allBicis.push(aBici);
}

bicicleta.findById = function(aBiciId){
    var aBici = bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`no existe una bicicleta con el id ${aBiciId}`);
}

bicicleta.removeById = function(aBiciId){
    for (var i = 0; i < bicicleta.allBicis.length; i++) {
        if(bicicleta.allBicis[i].id == aBiciId){
            bicicleta.allBicis.splice(i, 1);
            break;
        }      
    }
}

/* 
var a = new bicicleta(1,'rojo','urbana', [-17.793284, -63.099103]);
var b = new bicicleta(2, 'azul','Montañera',[-17.798116, -63.099371]);

bicicleta.add(a);
bicicleta.add(b); */

module.exports = bicicleta;